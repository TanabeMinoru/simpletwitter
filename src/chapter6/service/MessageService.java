package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//簡易Twitter実践課題②selectの引数にString型のuserIdを追加
	public List<UserMessage> select(String userId) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			//簡易Twitter実践課題②idをnullで初期化
			Integer id = null;

			//簡易Twitter実践課題②servletからuserIdの値が渡ってきていたら整数型に型変換し、idに代入
			if(!StringUtils.isBlank(userId)) {
				id = Integer.parseInt(userId);
			}

			//簡易Twitter実践課題②messageDao.selectに引数としてInteger型のidを追加
			//idがnullだったら全件取得、idがnull以外だったらその値に対応するユーザーIDの投稿を取得
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM);

			commit(connection);

			return messages;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
